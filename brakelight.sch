EESchema Schematic File Version 4
LIBS:brakelight-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L brakelight-rescue:STCS2SPR-STCS2SPR IC1
U 1 1 5BBD3007
P 5800 1500
F 0 "IC1" H 6500 1765 50  0000 C CNN
F 1 "STCS2SPR" H 6500 1674 50  0000 C CNN
F 2 "STCS2SPR:SOIC127P1410X370-11N" H 7050 1600 50  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/6d/9e/b0/b4/4b/36/48/91/CD00172746.pdf/files/CD00172746.pdf/jcr:content/translations/en.CD00172746.pdf" H 7050 1500 50  0001 L CNN
F 4 "STMICROELECTRONICS - STCS2SPR - LED DRIVER, CC, POWERSO-10" H 7050 1400 50  0001 L CNN "Description"
F 5 "3.7" H 7050 1300 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 7050 1200 50  0001 L CNN "Manufacturer_Name"
F 7 "STCS2SPR" H 7050 1100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 7050 1000 50  0001 L CNN "RS Part Number"
F 9 "" H 7050 900 50  0001 L CNN "RS Price/Stock"
F 10 "STCS2SPR" H 7050 800 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/stcs2spr/stmicroelectronics" H 7050 700 50  0001 L CNN "Arrow Price/Stock"
	1    5800 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR03
U 1 1 5BBD3171
P 5650 1000
F 0 "#PWR03" H 5650 850 50  0001 C CNN
F 1 "+24V" H 5665 1173 50  0000 C CNN
F 2 "" H 5650 1000 50  0001 C CNN
F 3 "" H 5650 1000 50  0001 C CNN
	1    5650 1000
	1    0    0    -1  
$EndComp
Text GLabel 5600 1900 0    50   Input ~ 0
Driver_Drain
$Comp
L Device:C_Small C2
U 1 1 5BBD58B8
P 5700 2100
F 0 "C2" H 5792 2146 50  0000 L CNN
F 1 "0.47u" H 5792 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5700 2100 50  0001 C CNN
F 3 "~" H 5700 2100 50  0001 C CNN
	1    5700 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1800 7250 1800
Wire Wire Line
	7250 1800 7250 1900
Wire Wire Line
	7250 1900 7200 1900
$Comp
L Device:R_Small R2
U 1 1 5BBD5C29
P 7250 2050
F 0 "R2" H 7191 2004 50  0000 R CNN
F 1 "0.3R" H 7191 2095 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 7250 2050 50  0001 C CNN
F 3 "~" H 7250 2050 50  0001 C CNN
	1    7250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1900 7250 1950
Connection ~ 7250 1900
$Comp
L power:GND #PWR07
U 1 1 5BBD6152
P 7250 2250
F 0 "#PWR07" H 7250 2000 50  0001 C CNN
F 1 "GND" H 7255 2077 50  0000 C CNN
F 2 "" H 7250 2250 50  0001 C CNN
F 3 "" H 7250 2250 50  0001 C CNN
	1    7250 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2150 7250 2200
Wire Wire Line
	5600 1900 5700 1900
Wire Wire Line
	5700 2000 5700 1900
Connection ~ 5700 1900
Wire Wire Line
	5700 1900 5800 1900
$Comp
L power:GND #PWR08
U 1 1 5BBD6D80
P 5700 2300
F 0 "#PWR08" H 5700 2050 50  0001 C CNN
F 1 "GND" H 5705 2127 50  0000 C CNN
F 2 "" H 5700 2300 50  0001 C CNN
F 3 "" H 5700 2300 50  0001 C CNN
	1    5700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2200 5700 2300
$Comp
L power:GND #PWR09
U 1 1 5BBD74E4
P 6500 2450
F 0 "#PWR09" H 6500 2200 50  0001 C CNN
F 1 "GND" H 6505 2277 50  0000 C CNN
F 2 "" H 6500 2450 50  0001 C CNN
F 3 "" H 6500 2450 50  0001 C CNN
	1    6500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2400 6500 2450
$Comp
L Device:R_Small R1
U 1 1 5BBD7F86
P 5650 1350
F 0 "R1" H 5591 1304 50  0000 R CNN
F 1 "100R" H 5591 1395 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 5650 1350 50  0001 C CNN
F 3 "~" H 5650 1350 50  0001 C CNN
	1    5650 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 1200 5650 1250
Wire Wire Line
	5650 1500 5650 1450
Wire Wire Line
	5650 1600 5650 1500
Connection ~ 5650 1500
Wire Wire Line
	7500 1700 7500 2200
Connection ~ 7250 2200
Wire Wire Line
	7250 2250 7250 2200
Wire Wire Line
	7200 1700 7500 1700
Wire Wire Line
	7250 2200 7500 2200
Wire Wire Line
	5800 1500 5650 1500
Wire Wire Line
	5800 1600 5650 1600
$Comp
L power:GND #PWR06
U 1 1 5BBEC10C
P 4500 2100
F 0 "#PWR06" H 4500 1850 50  0001 C CNN
F 1 "GND" H 4505 1927 50  0000 C CNN
F 2 "" H 4500 2100 50  0001 C CNN
F 3 "" H 4500 2100 50  0001 C CNN
	1    4500 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5BBF02AB
P 4500 1800
F 0 "C1" H 4592 1846 50  0000 L CNN
F 1 "0.1u" H 4592 1755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4500 1800 50  0001 C CNN
F 3 "~" H 4500 1800 50  0001 C CNN
	1    4500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1500 4500 1700
Wire Wire Line
	4500 1900 4500 2100
Text GLabel 4950 1700 0    50   Input ~ 0
ENABLE
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED1
U 1 1 5BC586E5
P 2000 4150
F 0 "LED1" H 2500 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 2500 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 2850 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 2850 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 2850 4050 50  0001 L CNN "Description"
F 5 "2.41" H 2850 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 2850 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 2850 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 2850 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 2850 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 2850 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 2850 3350 50  0001 L CNN "Arrow Price/Stock"
	1    2000 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 4150 2000 4150
Wire Wire Line
	2000 4050 2000 3900
Wire Wire Line
	2000 3900 1000 3900
Wire Wire Line
	1000 3900 1000 4150
Wire Wire Line
	1000 4150 900  4150
Connection ~ 1000 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED2
U 1 1 5BC62FF4
P 3100 4150
F 0 "LED2" H 3600 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 3600 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 3950 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 3950 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 3950 4050 50  0001 L CNN "Description"
F 5 "2.41" H 3950 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 3950 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 3950 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 3950 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 3950 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 3950 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 3950 3350 50  0001 L CNN "Arrow Price/Stock"
	1    3100 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 4150 3100 4150
Wire Wire Line
	3100 4050 3100 3900
Wire Wire Line
	3100 3900 2100 3900
Wire Wire Line
	2100 3900 2100 4150
Connection ~ 2100 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED3
U 1 1 5BC67159
P 4200 4150
F 0 "LED3" H 4700 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 4700 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 5050 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 5050 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 5050 4050 50  0001 L CNN "Description"
F 5 "2.41" H 5050 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 5050 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 5050 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 5050 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 5050 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 5050 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 5050 3350 50  0001 L CNN "Arrow Price/Stock"
	1    4200 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 4150 4200 4150
Wire Wire Line
	4200 4050 4200 3900
Wire Wire Line
	4200 3900 3200 3900
Wire Wire Line
	3200 3900 3200 4150
Connection ~ 3200 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED4
U 1 1 5BC6716E
P 5300 4150
F 0 "LED4" H 5800 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 5800 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 6150 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 6150 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 6150 4050 50  0001 L CNN "Description"
F 5 "2.41" H 6150 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 6150 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 6150 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 6150 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 6150 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 6150 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 6150 3350 50  0001 L CNN "Arrow Price/Stock"
	1    5300 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 4150 5300 4150
Wire Wire Line
	5300 4050 5300 3900
Wire Wire Line
	5300 3900 4300 3900
Wire Wire Line
	4300 3900 4300 4150
Connection ~ 4300 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED5
U 1 1 5BC6ACEC
P 6400 4150
F 0 "LED5" H 6900 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 6900 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 7250 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 7250 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 7250 4050 50  0001 L CNN "Description"
F 5 "2.41" H 7250 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 7250 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 7250 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 7250 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 7250 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 7250 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 7250 3350 50  0001 L CNN "Arrow Price/Stock"
	1    6400 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 4150 6400 4150
Wire Wire Line
	6400 4050 6400 3900
Wire Wire Line
	6400 3900 5400 3900
Wire Wire Line
	5400 3900 5400 4150
Connection ~ 5400 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED6
U 1 1 5BC6AD01
P 7500 4150
F 0 "LED6" H 8000 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 8000 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 8350 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 8350 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 8350 4050 50  0001 L CNN "Description"
F 5 "2.41" H 8350 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 8350 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 8350 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 8350 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 8350 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 8350 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 8350 3350 50  0001 L CNN "Arrow Price/Stock"
	1    7500 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 4150 7500 4150
Wire Wire Line
	7500 4050 7500 3900
Wire Wire Line
	7500 3900 6500 3900
Wire Wire Line
	6500 3900 6500 4150
Connection ~ 6500 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED7
U 1 1 5BC6AD15
P 8600 4150
F 0 "LED7" H 9100 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 9100 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 9450 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 9450 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 9450 4050 50  0001 L CNN "Description"
F 5 "2.41" H 9450 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 9450 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 9450 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 9450 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 9450 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 9450 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 9450 3350 50  0001 L CNN "Arrow Price/Stock"
	1    8600 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 4150 8600 4150
Wire Wire Line
	8600 4050 8600 3900
Wire Wire Line
	8600 3900 7600 3900
Wire Wire Line
	7600 3900 7600 4150
Connection ~ 7600 4150
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED8
U 1 1 5BC6AD29
P 9700 4150
F 0 "LED8" H 10200 3785 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 10200 3876 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 10550 4250 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 10550 4150 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 10550 4050 50  0001 L CNN "Description"
F 5 "2.41" H 10550 3950 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 10550 3850 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 10550 3750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 10550 3650 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 10550 3550 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 10550 3450 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 10550 3350 50  0001 L CNN "Arrow Price/Stock"
	1    9700 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	9800 4150 9700 4150
Wire Wire Line
	9700 4050 9700 3900
Wire Wire Line
	9700 3900 8700 3900
Wire Wire Line
	8700 3900 8700 4150
Connection ~ 8700 4150
Text GLabel 9800 4150 2    50   Input ~ 0
Driver_Drain
$Comp
L power:+24V #PWR010
U 1 1 5BC6DBAD
P 900 4150
F 0 "#PWR010" H 900 4000 50  0001 C CNN
F 1 "+24V" H 915 4323 50  0000 C CNN
F 2 "" H 900 4150 50  0001 C CNN
F 3 "" H 900 4150 50  0001 C CNN
	1    900  4150
	1    0    0    -1  
$EndComp
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED9
U 1 1 5BC71800
P 2000 4700
F 0 "LED9" H 2500 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 2500 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 2850 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 2850 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 2850 4600 50  0001 L CNN "Description"
F 5 "2.41" H 2850 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 2850 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 2850 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 2850 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 2850 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 2850 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 2850 3900 50  0001 L CNN "Arrow Price/Stock"
	1    2000 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 4700 2000 4700
Wire Wire Line
	2000 4600 2000 4450
Wire Wire Line
	2000 4450 1000 4450
Wire Wire Line
	1000 4450 1000 4700
Wire Wire Line
	1000 4700 900  4700
Connection ~ 1000 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED10
U 1 1 5BC71815
P 3100 4700
F 0 "LED10" H 3600 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 3600 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 3950 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 3950 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 3950 4600 50  0001 L CNN "Description"
F 5 "2.41" H 3950 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 3950 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 3950 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 3950 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 3950 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 3950 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 3950 3900 50  0001 L CNN "Arrow Price/Stock"
	1    3100 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 4700 3100 4700
Wire Wire Line
	3100 4600 3100 4450
Wire Wire Line
	3100 4450 2100 4450
Wire Wire Line
	2100 4450 2100 4700
Connection ~ 2100 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED11
U 1 1 5BC71829
P 4200 4700
F 0 "LED11" H 4700 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 4700 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 5050 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 5050 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 5050 4600 50  0001 L CNN "Description"
F 5 "2.41" H 5050 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 5050 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 5050 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 5050 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 5050 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 5050 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 5050 3900 50  0001 L CNN "Arrow Price/Stock"
	1    4200 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 4700 4200 4700
Wire Wire Line
	4200 4600 4200 4450
Wire Wire Line
	4200 4450 3200 4450
Wire Wire Line
	3200 4450 3200 4700
Connection ~ 3200 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED12
U 1 1 5BC7183D
P 5300 4700
F 0 "LED12" H 5800 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 5800 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 6150 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 6150 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 6150 4600 50  0001 L CNN "Description"
F 5 "2.41" H 6150 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 6150 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 6150 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 6150 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 6150 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 6150 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 6150 3900 50  0001 L CNN "Arrow Price/Stock"
	1    5300 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 4700 5300 4700
Wire Wire Line
	5300 4600 5300 4450
Wire Wire Line
	5300 4450 4300 4450
Wire Wire Line
	4300 4450 4300 4700
Connection ~ 4300 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED13
U 1 1 5BC71851
P 6400 4700
F 0 "LED13" H 6900 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 6900 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 7250 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 7250 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 7250 4600 50  0001 L CNN "Description"
F 5 "2.41" H 7250 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 7250 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 7250 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 7250 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 7250 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 7250 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 7250 3900 50  0001 L CNN "Arrow Price/Stock"
	1    6400 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 4700 6400 4700
Wire Wire Line
	6400 4600 6400 4450
Wire Wire Line
	6400 4450 5400 4450
Wire Wire Line
	5400 4450 5400 4700
Connection ~ 5400 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED14
U 1 1 5BC71865
P 7500 4700
F 0 "LED14" H 8000 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 8000 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 8350 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 8350 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 8350 4600 50  0001 L CNN "Description"
F 5 "2.41" H 8350 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 8350 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 8350 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 8350 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 8350 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 8350 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 8350 3900 50  0001 L CNN "Arrow Price/Stock"
	1    7500 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 4700 7500 4700
Wire Wire Line
	7500 4600 7500 4450
Wire Wire Line
	7500 4450 6500 4450
Wire Wire Line
	6500 4450 6500 4700
Connection ~ 6500 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED15
U 1 1 5BC71879
P 8600 4700
F 0 "LED15" H 9100 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 9100 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 9450 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 9450 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 9450 4600 50  0001 L CNN "Description"
F 5 "2.41" H 9450 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 9450 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 9450 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 9450 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 9450 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 9450 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 9450 3900 50  0001 L CNN "Arrow Price/Stock"
	1    8600 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 4700 8600 4700
Wire Wire Line
	8600 4600 8600 4450
Wire Wire Line
	8600 4450 7600 4450
Wire Wire Line
	7600 4450 7600 4700
Connection ~ 7600 4700
$Comp
L brakelight-rescue:LR_H9GP-HZKX-1-LR_H9GP-HZKX-1 LED16
U 1 1 5BC7188D
P 9700 4700
F 0 "LED16" H 10200 4335 50  0000 C CNN
F 1 "LR_H9GP-HZKX-1" H 10200 4426 50  0000 C CNN
F 2 "LR_H9GP_HZKX_Fat_Pads:LRH9GPHZKX1_Fat_pads" H 10550 4800 50  0001 L CNN
F 3 "https://www.osram.com/media/resource/hires/osram-dam-2493664/LR%20H9GP.pdf" H 10550 4700 50  0001 L CNN
F 4 "Osram Opto LR H9GP-HZKX-1, OSLON Black Series Red LED, 632 nm, Round Lens SMD package" H 10550 4600 50  0001 L CNN "Description"
F 5 "2.41" H 10550 4500 50  0001 L CNN "Height"
F 6 "OSRAM Opto Semiconductors" H 10550 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "LR H9GP-HZKX-1" H 10550 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "9154771P" H 10550 4200 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/9154771P" H 10550 4100 50  0001 L CNN "RS Price/Stock"
F 10 "LR H9GP-HZKX-1" H 10550 4000 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/9154771P" H 10550 3900 50  0001 L CNN "Arrow Price/Stock"
	1    9700 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9800 4700 9700 4700
Wire Wire Line
	9700 4600 9700 4450
Wire Wire Line
	9700 4450 8700 4450
Wire Wire Line
	8700 4450 8700 4700
Connection ~ 8700 4700
Text GLabel 9800 4700 2    50   Input ~ 0
Driver_Drain
$Comp
L power:+24V #PWR011
U 1 1 5BC7189A
P 900 4700
F 0 "#PWR011" H 900 4550 50  0001 C CNN
F 1 "+24V" H 915 4873 50  0000 C CNN
F 2 "" H 900 4700 50  0001 C CNN
F 3 "" H 900 4700 50  0001 C CNN
	1    900  4700
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Male J1
U 1 1 5C11F5C0
P 2650 2300
F 0 "J1" H 2830 2346 50  0000 L CNN
F 1 "DB9_Male" H 2830 2255 50  0000 L CNN
F 2 "443976-1:4439761" H 2650 2300 50  0001 C CNN
F 3 " ~" H 2650 2300 50  0001 C CNN
	1    2650 2300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5C11F695
P 3000 2700
F 0 "#PWR0101" H 3000 2450 50  0001 C CNN
F 1 "GND" H 3005 2527 50  0000 C CNN
F 2 "" H 3000 2700 50  0001 C CNN
F 3 "" H 3000 2700 50  0001 C CNN
	1    3000 2700
	-1   0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0102
U 1 1 5C11F6CE
P 3000 1900
F 0 "#PWR0102" H 3000 1750 50  0001 C CNN
F 1 "+24V" H 3015 2073 50  0000 C CNN
F 2 "" H 3000 1900 50  0001 C CNN
F 3 "" H 3000 1900 50  0001 C CNN
	1    3000 1900
	-1   0    0    -1  
$EndComp
Text GLabel 3100 2200 2    50   Input ~ 0
ENABLE
Wire Wire Line
	3000 2100 2950 2100
Wire Wire Line
	2950 2000 3000 2000
Wire Wire Line
	2950 1900 3000 1900
Connection ~ 3000 1900
Wire Wire Line
	3000 2500 2950 2500
Wire Wire Line
	3000 2500 3000 2600
Wire Wire Line
	2950 2600 3000 2600
Connection ~ 3000 2600
Wire Wire Line
	3000 2600 3000 2700
Wire Wire Line
	3000 2700 2950 2700
Connection ~ 3000 2700
Wire Wire Line
	3100 2200 2950 2200
Wire Wire Line
	4500 1500 5650 1500
Wire Wire Line
	4950 1700 5000 1700
$Comp
L Device:R_Small R3
U 1 1 5C13F86E
P 5000 1900
F 0 "R3" H 4941 1854 50  0000 R CNN
F 1 "1k" H 4941 1945 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 5000 1900 50  0001 C CNN
F 3 "~" H 5000 1900 50  0001 C CNN
	1    5000 1900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C13F8DE
P 5000 2100
F 0 "#PWR0103" H 5000 1850 50  0001 C CNN
F 1 "GND" H 5005 1927 50  0000 C CNN
F 2 "" H 5000 2100 50  0001 C CNN
F 3 "" H 5000 2100 50  0001 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2000 5000 2100
Wire Wire Line
	5000 1800 5000 1700
Connection ~ 5000 1700
Wire Wire Line
	5000 1700 5800 1700
Connection ~ 3000 2000
Wire Wire Line
	3000 1900 3000 2000
Wire Wire Line
	3000 2000 3000 2100
$Comp
L Device:R_Small R4
U 1 1 5C148A76
P 5650 1100
F 0 "R4" H 5591 1054 50  0000 R CNN
F 1 "0R" H 5591 1145 50  0000 R CNN
F 2 "Resistor_SMD:R_1806_4516Metric" H 5650 1100 50  0001 C CNN
F 3 "~" H 5650 1100 50  0001 C CNN
	1    5650 1100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5C148E5A
P 3400 1850
F 0 "H1" H 3500 1896 50  0000 L CNN
F 1 "MountingHole" H 3500 1805 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_Pad" H 3400 1850 50  0001 C CNN
F 3 "~" H 3400 1850 50  0001 C CNN
	1    3400 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C148EDD
P 3600 1650
F 0 "H2" H 3700 1696 50  0000 L CNN
F 1 "MountingHole" H 3700 1605 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_Pad" H 3600 1650 50  0001 C CNN
F 3 "~" H 3600 1650 50  0001 C CNN
	1    3600 1650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
